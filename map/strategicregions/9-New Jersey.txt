
strategic_region={
	id=9
	name="STRATEGICREGION_9"
	provinces={
		195 2491 2826 2908 3687 4252 4329 5305 5433 5836 5954 6004 6090 6188 6226 6833 7953 7976 7984 7995 8001 8002 8068 8076 8077 8085 8107 8118 8160 8161 8174 8191 8192 8209 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
