
strategic_region={
	id=12
	name="STRATEGICREGION_12"
	provinces={
		563 787 1923 2841 3117 3495 3532 3919 4200 4370 5049 5198 5482 5496 5764 5972 6029 6180 6294 6540 6601 6638 6777 6809 6810 6865 6877 8190 8199 8208 8233 8246 8258 8261 8271 8278 8301 8306 8309 8334 8340 8365 8371 8398 8401 8411 8413 8436 8437 8454 8460 
	}
	weather={
		period={
			between={ 0.0 30.0 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.1 27.1 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.2 30.2 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.3 29.3 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.4 30.4 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.5 29.5 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.6 30.6 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.7 30.7 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.8 29.8 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.9 30.9 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.10 29.10 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
		period={
			between={ 0.11 30.11 }
			temperature={ -10.0 35.0 }
			temperature_day_night={ 2.0 -2.0 }
			no_phenomenon=1.500
			rain_light=0.250
			rain_heavy=0.100
			snow=0.000
			blizzard=0.000
			arctic_water=0.000
			mud=0.000
			sandstorm=0.000
			min_snow_level=0.000
		}
	}
}
