﻿capital = 1

#	BASIC VALUES
oob = "generic_1858"
set_research_slots = 2
set_stability = 0.5
set_war_support = 0.5
set_convoys = 100
add_offsite_building = { type = industrial_complex level = 0 }
add_offsite_building = { type = arms_factory level = 0 }

###	TECHNOLOGY
set_technology = {
	infantry_weapons = 1
}

### VARIABLES

###	IDEAS
add_ideas = {
	united_states_state
	free_state
}

###	POLITICS
set_politics = {
	ruling_party = union
	last_election = "1858.1.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = { 
	union = 99 confederacy = 0 separatism = 1 neutrality = 0
}

###	OPINION & TRADE MODIFIERS

###	POLITICIANS
create_country_leader = {
	name = "William A. Newell"
	desc = "desc_WILLIAM_A_NEWELL"
	picture = "William_A_Newell.dds"
	ideology = northern_republican
}

###	GENERALS

###	ADMIRALS

### OPERATIVES