state={
	id=423
	name="STATE_423" #Wright Laclede Camden Pulaski
	provinces={
		804 1785 5968 5995 6704 8720 8733 8743 8776
	}
	manpower=17864
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MIS
		add_core_of = MIS
		buildings = {
			infrastructure = 4
		}
	}
}