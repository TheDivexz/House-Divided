state={
	id=107
	name="STATE_107" #Dutchess Putnam Rockland Orange
	provinces={
		617 6119 6132 6549 6622 7765 7767 7787 7789 7800 7814 7817 7836 7862
	}
	manpower=160352
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = NYK
		add_core_of = NYK
		buildings = {
			infrastructure = 4
		}
	}
}