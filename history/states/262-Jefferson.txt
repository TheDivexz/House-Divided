state={
	id=262
	name="STATE_262" #Clark Scott Washington Jefferson
	provinces={
		173 1907 2166 6393 6602 8569
	}
	manpower=69529
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = IND
		add_core_of = IND
		buildings = {
			infrastructure = 4
		}
	}
}