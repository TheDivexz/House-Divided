state={
	id=327
	name="STATE_327" #Hale Perry Chilton Bibb Tuscaloosa Pickens
	provinces={
		317 1888 2518 4727 4743 4846 5695 9401 9413 9430 9437 9448 9453 9458 9474 9475
	}
	manpower=40594
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = ALA
		add_core_of = ALA
		buildings = {
			infrastructure = 4
		}
	}
}