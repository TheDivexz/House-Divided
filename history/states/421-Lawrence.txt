state={
	id=421
	name="STATE_421" #Jasper Barton Lawrence Dade
	provinces={
		2149 2280 5677 6308 6607 8791 8845 8859 8876
	}
	manpower=23609
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MIS
		add_core_of = MIS
		buildings = {
			infrastructure = 4
		}
	}
}