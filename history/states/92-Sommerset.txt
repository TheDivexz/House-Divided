state={
	id=92
	name="STATE_92" #Piscataquis Somerset Oxford Franklin
	provinces={
		169 1077 2383 4304 4650 7362
		247 387 500 1385 1406 2924 6164 7392
		1829 6557 6712 7453
		3355 4690
	}
	manpower=108864
	state_category = rural
	buildings_max_level_factor=1.000

	history = {
		owner = MAI
		add_core_of = MAI
		buildings = {
			infrastructure = 2
		}
	}
}