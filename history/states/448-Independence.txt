state={
	id=448
	name="STATE_448" #Lawrence Sharp Independence Fulton Izard
	provinces={ 
		1192 1242 1828 4086 5474 5538 6337 9008 9065
	}
	manpower=32612
	state_category = large_city
	buildings_max_level_factor=1.000
	history = {
		owner = ARK
		add_core_of = ARK
		buildings = {
			infrastructure = 4
		}
	}
}