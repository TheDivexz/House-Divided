state={
	id=412
	name="STATE_412" #Palo_Alto Kossuth Winnebago Hancock
	provinces={
		686 2197 2481 3773 7873 7904
	}
	manpower=895
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = IOW
		add_core_of = IOW
		buildings = {
			infrastructure = 4
		}
	}
}