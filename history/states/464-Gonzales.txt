state={
	id=464
	name="STATE_464" #Kames Dewitt Wilson Gonzeles Atascosa
	provinces={
		466 1286 2350 2531 2795 3968 4583 9935 9943
	}
	manpower=11671
	state_category = large_city
	buildings_max_level_factor=1.000
	history = {
		owner = TEX
		add_core_of = TEX
		buildings = {
			infrastructure = 4
		}
	}
}