state={
	id=339
	name="STATE_339" #Jasper Clarke Lauderdale Newton
	provinces={
		693 2421 4501 5747 5802 5941 9544 9609 9623 9633
	}
	manpower=26648
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MSP
		add_core_of = MSP
		buildings = {
			infrastructure = 4
		}
	}
}