state={
	id=498
	name="STATE_498" #Alameda Contra_Costa Solano Yolo
	provinces={
		2020 2759 3785 4206 4778 4913 5337 6849 8256 8279 8280 8304 8310 8341 8367 
	}
	manpower=25508
	state_category = large_city
	buildings_max_level_factor=1.000
	history = {
		owner = CAL
		add_core_of = CAL
		buildings = {
			infrastructure = 4
		}
	}
}