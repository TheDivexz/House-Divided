state={
	id=217
	name="STATE_217" #Lanier Cook Lowndes Brooks Colquitt Worth
	provinces={
		2080 3891 5947 9621 9674 9677 9691
	}
	manpower=9230
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = GRG
		add_core_of = GRG
		buildings = {
			infrastructure = 4
		}
	}
}