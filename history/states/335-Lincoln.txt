state={
	id=335
	name="STATE_335" #Marion Jefferson_Davis Walthall Lawrence Pike Lincoln Amite
	provinces={
		3095 3708 4040 4909 9663 9719
		1095 4761 5200 6443 9705 9718 9725 9734
	}
	manpower=18614
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MSP
		add_core_of = MSP
		buildings = {
			infrastructure = 4
		}
	}
}