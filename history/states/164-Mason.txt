state={
	id=164
	name="STATE_164" #Lincoln Cabell Putnam Mason Jackson
	provinces={
		456 2480 5328 5814 8486 8530 8552
	}
	manpower=30389
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = VIR
		add_core_of = VIR
		add_core_of = WVI
		buildings = {
			infrastructure = 4
		}
	}
}