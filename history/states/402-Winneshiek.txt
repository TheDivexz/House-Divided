state={
	id=402
	name="STATE_402" #Allamakee Winneshiek Fayette Buchanan
	provinces={
		2931 3034 3810 4083 6847 7968
	}
	manpower=46096
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = IOW
		add_core_of = IOW
		buildings = {
			infrastructure = 4
		}
	}
}