state={
	id=185
	name="STATE_185" #Spartanburg Cherokee Union York Lancaster
	provinces={
		796 3025 3756 5444 5499 5786 6289 6453 9048 9049 9059 9067 9096 9106
		4788 5626
	}
	manpower=44590
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = SCR
		add_core_of = SCR
		buildings = {
			infrastructure = 4
		}
	}
}