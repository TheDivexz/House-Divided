state={
	id=317
	name="STATE_317" #Stewart Houston Henry Benton Carroll
	provinces={
		490 1305 3013 3320 3914 4857 6115 8913
	}
	manpower=42253
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = TEN
		add_core_of = TEN
		buildings = {
			infrastructure = 4
		}
	}
}