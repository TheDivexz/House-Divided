state={
	id=269
	name="STATE_269" #Johnson Shelby Marion Hancock
	provinces={
		2943 5117 5231 6580 6812 8417 8467
	}
	manpower=86122
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = IND
		add_core_of = IND
		buildings = {
			infrastructure = 4
		}
	}
}