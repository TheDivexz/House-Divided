state={
	id=250
	name="STATE_250" #Monroe Wayne Macomb Oakland
	provinces={
		861 2716 3055 3178 3895 4470 4960 5426 7830 7842 7887 7916 7938 7965
	}
	manpower=156170
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MIC
		add_core_of = MIC
		buildings = {
			infrastructure = 4
		}
	}
}