state={
	id=302
	name="STATE_302" #Cocke Jefferson Hamblen Sevier Hawkins
	provinces={
		1528 3397 4094 4592 4845 6318 6512 6668 8820 8931 8948 8971
	}
	manpower=45811
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = TEN
		add_core_of = TEN
		buildings = {
			infrastructure = 4
		}
	}
}