state={
	id=57
	name="STATE_57" #Lake Obion Weakley Gibson Crockett
	provinces={
		4749 5146 5659 8939 8942 8953 8994 9028 9052 9058 
	}
	manpower=39910
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = TEN
		add_core_of = TEN
		buildings = {
			infrastructure = 4
		}
	}
}
