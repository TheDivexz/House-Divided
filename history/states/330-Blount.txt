state={
	id=330
	name="STATE_330" #Jefferson Blount Walker Cullman Winston
	provinces={
		576 1063 3332 3578 4632 5053 5115 5443 6050 9283 9287 9294 9311 9328 9347 9353 9370 9403
	}
	manpower=30186
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = ALA
		add_core_of = ALA
		buildings = {
			infrastructure = 4
		}
	}
}