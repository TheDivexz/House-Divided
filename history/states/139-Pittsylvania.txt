state={
	id=139
	name="STATE_139" #Floyd Franklin Henry Pittsylvania
	provinces={
		3280 4604 4724 4921 6611 6679 6680 6758 8693 8745 8765
	}
	manpower=45265
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = VIR
		add_core_of = VIR
		buildings = {
			infrastructure = 4
		}
	}
}