state={
	id=102
	name="STATE_102" #Jefferson Lewis Oswego Oneida
	provinces={
		307 1899 3435 4117 4488 4657 4766 4854 5818 5857 7525 7536 7544 7548 7562 7574 7616 7621 7641
	}
	manpower=276338
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = NYK
		add_core_of = NYK
		buildings = {
			infrastructure = 4
		}
	}
}