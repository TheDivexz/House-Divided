state={
	id=496
	name="STATE_496" #Merced Stanislaus Tulumne Calaveras
	provinces={
		1204 3075 3442 3641 3915 3963 4130 5326 5408 5479 6172 6749 8320 8352 8370 8377 8382 8384 8399 8432 8449 8458 8490
	}
	manpower=18924
	state_category = large_city
	buildings_max_level_factor=1.000
	history = {
		owner = CAL
		add_core_of = CAL
		buildings = {
			infrastructure = 4
		}
	}
}