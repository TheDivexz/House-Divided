state={
	id=64
	name="STATE_64" #DeSoto Tate Panola Quitman Coahoma Tunica
	provinces={
		1633 2215 3251 3380 3691 5681 6751 9243 9270 9302 9319 9324 
	}
	manpower=16990
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MSP
		add_core_of = MSP
		buildings = {
			infrastructure = 4
		}
	}
}
