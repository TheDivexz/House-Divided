state={
	id=237
	name="STATE_237" #Morrow Marion Crawford Wyandot
	provinces={
		744 5010 5334 8150
	}
	manpower=84192
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = OHI
		add_core_of = OHI
		buildings = {
			infrastructure = 4
		}
	}
}