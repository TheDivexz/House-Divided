state={
	id=437
	name="STATE_437" #Ray Clay Caldwell Clinton
	provinces={
		21 1149 5455 6797 8533
	}
	manpower=33058
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MIS
		add_core_of = MIS
		buildings = {
			infrastructure = 4
		}
	}
}