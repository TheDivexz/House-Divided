state={
	id=140
	name="STATE_140" #bEDFORD Botetourt Alleghany Campbell
	provinces={
		1534 2937 4875 5509 5772 6624 6892 8639
	}
	manpower=42060
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = VIR
		add_core_of = VIR
		buildings = {
			infrastructure = 4
		}
	}
}