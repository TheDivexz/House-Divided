state={
	id=429
	name="STATE_429" #Hickory Benton Saint_Clair Cedar
	provinces={
		1701 3189 3868 6258 6530 6755 6923 8714 8738
	}
	manpower=25603
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MIS
		add_core_of = MIS
		buildings = {
			infrastructure = 4
		}
	}
}