state={
	id=141
	name="STATE_141" #Bath Highland Augusta Rockbridge
	provinces={
		1084 3153 4706 6843 8518
	}
	manpower=40840
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = VIR
		add_core_of = VIR
		buildings = {
			infrastructure = 4
		}
	}
}