state={
	id=135
	name="STATE_135" #Cecil Kent Queen_Anne's Talbot
	provinces={
		1923 3117 6294 8199 8261 8233
	}
	manpower=43862
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MAR
		add_core_of = MAR
		buildings = {
			infrastructure = 4
		}
	}
}