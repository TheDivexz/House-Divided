state={
	id=359
	name="STATE_359" #Union Alexander Pulaski Johnson
	provinces={
		4965 5550 8789 8809
	}
	manpower=29007
	state_category = large_city
	buildings_max_level_factor=1.000
	
	history = {
		owner = ILL
		add_core_of = ILL
		buildings = {
			infrastructure = 4
		}
	}
}