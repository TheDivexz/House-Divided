state={
	id=174
	name="STATE_174" #Scotland Hoke Robeson Cumberland Bladen Columbus
	provinces={
		1469 2221 2977 3700 4193 6436 8949 8967 8995 8996 9032 9046 9064
		282 593 961 2562 9033 9068 9076 6631
	}
	manpower=30138
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = NCR
		add_core_of = NCR
		buildings = {
			infrastructure = 4
		}
	}
}