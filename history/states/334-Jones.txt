state={
	id=334
	name="STATE_334" #Perry Forrest JOnes Wayne Lamar Covington
	provinces={
		323 487 2299 3535 6154 6364 6643 9652 9665 9711 9721 9733
	}
	manpower=9363
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MSP
		add_core_of = MSP
		buildings = {
			infrastructure = 4
		}
	}
}