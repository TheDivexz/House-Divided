state={
	id=388
	name="STATE_388" #Washington Chisago Ramset Hennepin
	provinces={
		2270 4767 5500 6469 7585 7610 7632 7634
	}
	manpower=32687
	state_category = large_city
	buildings_max_level_factor=1.000
	
	history = {
		owner = MIN
		add_core_of = MIN
		buildings = {
			infrastructure = 4
		}
	}
}