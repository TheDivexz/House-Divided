state={
	id=130
	name="STATE_130" #Erie Crawford Warren Forest
	provinces={
		962 2712 2727 4898 5619 6218 6376 7949 7884 7908 7944
	}
	manpower=117861
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = PEN
		add_core_of = PEN
		buildings = {
			infrastructure = 4
		}
	}
}