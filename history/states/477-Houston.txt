state={
	id=477
	name="STATE_477"  #Leon Madison Freestone Houston
	provinces={
		1770 2441 3359 3437 6025 6346 6492 9671 9737 9751 9752 9775
	}
	manpower=14231
	state_category = large_city
	buildings_max_level_factor=1.000
	history = {
		owner = TEX
		add_core_of = TEX
		buildings = {
			infrastructure = 4
		}
	}
}