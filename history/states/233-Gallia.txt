state={
	id=233
	name="STATE_233" #Gallia Lawrence Vinton Jackson
	provinces={
		3887 4205 5636 5639 5907 8496 8524
	}
	manpower=82740
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = OHI
		add_core_of = OHI
		buildings = {
			infrastructure = 4
		}
	}
}