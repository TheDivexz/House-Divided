state={
	id=249
	name="STATE_249" #Hillsdale Lenawee Jackson Washtenaaw
	provinces={
		36 2691 4095 5463 5703 5816 6189 7902 7915 7924 7933 7951 7973
	}
	manpower=125038
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = MIC
		add_core_of = MIC
		buildings = {
			infrastructure = 4
		}
	}
}