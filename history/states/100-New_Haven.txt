state={
	id=100
	name="STATE_100" #Fairfield New_Haven Litchfield Hartford
	provinces={
		625 1161 2930 3883 4035 4578 4910 5637 5664 6157 6931 7735 7749 7755 7805 7849
	}
	manpower=305873
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = CNC
		add_core_of = CNC
		buildings = {
			infrastructure = 4
		}
	}
}