state={
	id=147
	name="STATE_147" #York James_City Charles_City Chesterfield New_Kent
	provinces={
		4734 8570 8574
	}
	manpower=18479
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = VIR
		add_core_of = VIR
		buildings = {
			infrastructure = 4
		}
	}
}