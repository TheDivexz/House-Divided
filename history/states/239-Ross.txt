state={
	id=239
	name="STATE_239" #Hocking Pickaway Ross Pike
	provinces={
		632 3741 6519 8388 8418 8392 8452
	}
	manpower=84459
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = OHI
		add_core_of = OHI
		buildings = {
			infrastructure = 4
		}
	}
}