state={
	id=358
	name="STATE_358" #St.Croix Polk Barron Burnett
	provinces={
		1094 2794 2855 4719 5946 6486 6535 7557 7584 7601 7622
	}
	manpower=6802
	state_category = large_city
	buildings_max_level_factor=1.000
	
	history = {
		owner = WIS
		add_core_of = WIS
		buildings = {
			infrastructure = 4
		}
	}
}
