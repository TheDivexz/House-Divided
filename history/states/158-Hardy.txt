state={
	id=158
	name="STATE_158" #Hardy Mineral Grant Tucker Pendleton Randolph
	provinces={
		433 2936 2971 4291 5059 6363 5190 8319 8425 8477
	}
	manpower=20576
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = VIR
		add_core_of = VIR
		add_core_of = WVI
		buildings = {
			infrastructure = 4
		}
	}
}