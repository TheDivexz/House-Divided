state={
	id=236
	name="STATE_236" #Ottawa Sandusky Seneca Lucas
	provinces={
		314 874 3481 6460 7983 8032
	}
	manpower=84687
	state_category = large_city
	buildings_max_level_factor=1.000

	history = {
		owner = OHI
		add_core_of = OHI
		buildings = {
			infrastructure = 4
		}
	}
}