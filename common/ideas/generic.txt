ideas = {

	country = { 
		
		united_states_state = {

			picture = american_patriotism
			allowed = { always = no }
			removal_cost = -1
			rule = {
				can_create_factions = no
				can_join_factions = yes
			}

		}
		
		united_states_federal_government = {

			picture = us_congress
			allowed = { always = no }
			removal_cost = -1
			modifier = {
				separatism_drift = -0.03
			}
			rule = {
				can_create_factions = yes
				can_join_factions = no
			}

		}
		
		free_state = {

			picture = volunteers
			allowed = { always = no }
			removal_cost = -1
			modifier = {
				stability_factor = 0.05
				union_drift = 0.01
			}

		}
		
		slave_state = {

			picture = segregation
			allowed = { always = no }
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.1
				stability_factor = -0.1
				war_support_factor = -0.1
				confederacy_drift = 0.01
			}

		}

	}

}
