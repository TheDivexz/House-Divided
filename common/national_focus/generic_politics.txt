
shared_focus = {
	id = GNRC_POL_cabinet_reform
	icon = self_management
	cost = 10
	x = 6
	y = 0
	completion_reward = {
		add_political_power = 100
	}
}
shared_focus = {
	id = GNRC_POL_social_appeasement
	icon = national_unity
	cost = 10
	x = -2
	y = 1
	relative_position_id = GNRC_POL_cabinet_reform
	prerequisite = { focus = GNRC_POL_cabinet_reform }
	completion_reward = {
		add_stability = 0.05
	}
}
shared_focus = {
	id = GNRC_POL_social_mobilization
	icon = demand_territory
	cost = 10
	x = 0
	y = 1
	relative_position_id = GNRC_POL_cabinet_reform
	prerequisite = { focus = GNRC_POL_cabinet_reform }
	completion_reward = {
		add_war_support = 0.05
	}
}
