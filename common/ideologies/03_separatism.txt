ideologies = {
	
	separatism = {
	
		types = {
			separatist = {}
			regionalist = {}
			localist = {}
			texan_nationalist = {}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_SEPARATISM_1"
			"FACTION_NAME_SEPARATISM_2"
			"FACTION_NAME_SEPARATISM_3"
			"FACTION_NAME_SEPARATISM_4"
		}
		
		color = { 0 160 0 }
		
		rules = {
			can_send_volunteers = yes
			can_puppet = no
			can_force_government = no
			can_declare_war_on_same_ideology = no
		}
		
		war_impact_on_world_tension = 1
		faction_impact_on_world_tension = 1
		
		modifiers = {
			generate_wargoal_tension = 1.00
		}
		
		faction_modifiers = {
			faction_trade_opinion_factor = 0.25
			justify_war_goal_when_in_major_war_time = -0.25
		}
	}
	
}