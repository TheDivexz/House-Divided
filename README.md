# A mod for Hearts of Iron 4 that simulates the US Civil War

##Developers
in order to contribute to this project you must do the following. 
1. Go to the Issues Tab and comment on the issue you would like to work on
2. Create a sperate branch and make your changes there 
    - DO NOT MAKE CHANGES TO THE MAIN BRANCH
    - if you are not a devloper, fork the repository and create a branch on that fork, and work from there
    - if you are working on the same issue as someone else, use the same branch as your peers to reduce clutter
3. When you want to submit your changes, create a pull request and link it to the appropriate issue
4. Once Didi/Divexz approves the changes, he will merge it 